unit data;

interface

uses
  System.SysUtils, System.Classes, FireDAC.Stan.Intf, inifiles,
  FireDAC.Stan.Option, FireDAC.Stan.Error, FireDAC.UI.Intf, FireDAC.Phys.Intf,
  FireDAC.Stan.Def, FireDAC.Stan.Pool, FireDAC.Stan.Async, FireDAC.Phys,
  FireDAC.Phys.MSSQL, FireDAC.Phys.MSSQLDef, FireDAC.FMXUI.Wait, Data.DB,
  FireDAC.Comp.Client, FMX.Forms, FireDAC.Comp.UI, FireDAC.DApt,
  FireDAC.VCLUI.Wait, FireDAC.VCLUI.Error, FireDAC.Stan.Param, FireDAC.DatS,
  FireDAC.DApt.Intf, FireDAC.Comp.DataSet;

type
  Tdm = class(TDataModule)
    database1: TFDConnection;
    FDQuery1: TFDQuery;
    FDQuery1NO_ORDEN: TIntegerField;
    FDQuery1NO_COTIZ: TIntegerField;
    FDQuery1NO_GEN: TIntegerField;
    FDQuery1FECHA: TSQLTimeStampField;
    FDQuery1PERIODO: TIntegerField;
    FDQuery1VEND_CODE: TStringField;
    FDQuery1BUS_NAME: TStringField;
    FDQuery1ADDRESS: TStringField;
    FDQuery1PHONE: TStringField;
    FDQuery1PHONE2: TStringField;
    FDQuery1FAX: TStringField;
    FDQuery1CONTACT: TStringField;
    FDQuery1BEEPER: TStringField;
    FDQuery1CEL_CONT: TStringField;
    FDQuery1TERMS_CODE: TStringField;
    FDQuery1EMAIL_CONT: TStringField;
    FDQuery1WH_CRIT: TIntegerField;
    FDQuery1WH_WHERE: TIntegerField;
    FDQuery1WH_NAME: TIntegerField;
    FDQuery1TOTAL: TFloatField;
    FDQuery1LINE_NO: TIntegerField;
    FDQuery1ITEM_CODE: TStringField;
    FDQuery1DESC1: TStringField;
    FDQuery1P_UNIT: TStringField;
    FDQuery1CANTIDAD: TIntegerField;
    FDQuery1EXIST: TIntegerField;
    FDQuery1COSTO: TFloatField;
    FDQuery1PAGE_NO: TIntegerField;
    FDQuery1ULT_VTA: TSQLTimeStampField;
    FDQuery1VTA_PROM: TIntegerField;
    FDQuery1USERNAME: TStringField;
    FDQuery1OFERTA: TIntegerField;
    FDQuery1RANK_NO: TIntegerField;
    FDQuery1RANK_PCT: TBCDField;
    FDQuery1DESCTO_CMP: TIntegerField;
    FDQuery1QTY_OFERTA: TIntegerField;
    FDQuery1PCT_BENEF: TBCDField;
    FDQuery1PCT_BENEFTOTAL: TIntegerField;
    FDQuery1ITEM_STATUS: TStringField;
    FDQuery1F_MERCADO: TIntegerField;
    FDQuery1IND_OFERTA: TIntegerField;
    FDQuery1QTY_ORDEN: TIntegerField;
    FDQuery1MES: TIntegerField;
    FDQuery1YY: TIntegerField;
    FDQuery1PCTDESCTONEGO: TIntegerField;
    FDQuery1FACTOR: TIntegerField;
    FDQuery1TOTAL_PROD: TFloatField;
    FDQuery1BARCODE: TStringField;
    FDQuery1LOCK_BIN_NO: TIntegerField;
    FDQuery1CO_NAME: TStringField;
    FDQuery1RNC: TStringField;
    FDQuery1CO_PHONE1: TStringField;
    FDQuery1CO_PHONE2: TStringField;
    FDQuery1CO_FAX: TStringField;
    FDQuery1ADDR1: TStringField;
    FDQuery1CITY: TStringField;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dm: Tdm;

implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

{$R *.dfm}

procedure Tdm.DataModuleCreate(Sender: TObject);
Var servidor, ConStr, login, database : String;
    IniFile: TIniFile;
begin

  IniFile  := TIniFile.Create(System.SysUtils.GetCurrentDir+'\aplisoft.ini');
  Database := IniFile.ReadString('DATABASESERVER','DATABASENAME','');
  Servidor := IniFile.ReadString('DATABASESERVER','SQLSERVERNAME','');

  try
   if Database = '' then
     Raise Exception.Create('ERROR: al cargar Base de Datos');
  except
   on E: Exception do
    begin
      Application.ShowException(E);
    //  fAreError := true;
      Exit;
    end;
  end;

  try
    database1.Params.Values['Server']  := Servidor;
    database1.Params.Values['Database'] := Database;

  except
    on E: Exception do
    begin
      Application.ShowException(E.Create('Ha ocurrido un error al conectarse a la Base de Datos!'));
    //  fAreError := true;
      Exit;

    end;
  end;

end;

end.
