object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 342
  Width = 523
  object database1: TFDConnection
    Params.Strings = (
      'SERVER=10.0.10.251,14335'
      'User_Name=sa'
      'Password=123456'
      'ApplicationName=Enterprise/Architect/Ultimate'
      'Workstation=PIAR'
      'DATABASE=farmapv'
      'MARS=yes'
      'DriverID=MSSQL')
    LoginPrompt = False
    Left = 176
    Top = 24
  end
  object FDQuery1: TFDQuery
    Connection = database1
    SQL.Strings = (
      'select top 20 * from vista_ordencompra_piar')
    Left = 176
    Top = 150
    object FDQuery1NO_ORDEN: TIntegerField
      FieldName = 'NO_ORDEN'
      Origin = 'NO_ORDEN'
      Required = True
    end
    object FDQuery1NO_COTIZ: TIntegerField
      FieldName = 'NO_COTIZ'
      Origin = 'NO_COTIZ'
      ReadOnly = True
      Required = True
    end
    object FDQuery1NO_GEN: TIntegerField
      FieldName = 'NO_GEN'
      Origin = 'NO_GEN'
      ReadOnly = True
      Required = True
    end
    object FDQuery1FECHA: TSQLTimeStampField
      FieldName = 'FECHA'
      Origin = 'FECHA'
    end
    object FDQuery1PERIODO: TIntegerField
      FieldName = 'PERIODO'
      Origin = 'PERIODO'
    end
    object FDQuery1VEND_CODE: TStringField
      FieldName = 'VEND_CODE'
      Origin = 'VEND_CODE'
      Size = 10
    end
    object FDQuery1BUS_NAME: TStringField
      FieldName = 'BUS_NAME'
      Origin = 'BUS_NAME'
      Size = 100
    end
    object FDQuery1ADDRESS: TStringField
      FieldName = 'ADDRESS'
      Origin = 'ADDRESS'
      Size = 100
    end
    object FDQuery1PHONE: TStringField
      FieldName = 'PHONE'
      Origin = 'PHONE'
      Size = 12
    end
    object FDQuery1PHONE2: TStringField
      FieldName = 'PHONE2'
      Origin = 'PHONE2'
      Size = 12
    end
    object FDQuery1FAX: TStringField
      FieldName = 'FAX'
      Origin = 'FAX'
      Size = 12
    end
    object FDQuery1CONTACT: TStringField
      FieldName = 'CONTACT'
      Origin = 'CONTACT'
      Size = 50
    end
    object FDQuery1BEEPER: TStringField
      FieldName = 'BEEPER'
      Origin = 'BEEPER'
      Size = 12
    end
    object FDQuery1CEL_CONT: TStringField
      FieldName = 'CEL_CONT'
      Origin = 'CEL_CONT'
      Size = 12
    end
    object FDQuery1TERMS_CODE: TStringField
      FieldName = 'TERMS_CODE'
      Origin = 'TERMS_CODE'
      FixedChar = True
      Size = 5
    end
    object FDQuery1EMAIL_CONT: TStringField
      FieldName = 'EMAIL_CONT'
      Origin = 'EMAIL_CONT'
      Size = 100
    end
    object FDQuery1WH_CRIT: TIntegerField
      FieldName = 'WH_CRIT'
      Origin = 'WH_CRIT'
      ReadOnly = True
    end
    object FDQuery1WH_WHERE: TIntegerField
      FieldName = 'WH_WHERE'
      Origin = 'WH_WHERE'
      ReadOnly = True
    end
    object FDQuery1WH_NAME: TIntegerField
      FieldName = 'WH_NAME'
      Origin = 'WH_NAME'
      ReadOnly = True
    end
    object FDQuery1TOTAL: TFloatField
      FieldName = 'TOTAL'
      Origin = 'TOTAL'
      ReadOnly = True
    end
    object FDQuery1LINE_NO: TIntegerField
      FieldName = 'LINE_NO'
      Origin = 'LINE_NO'
      Required = True
    end
    object FDQuery1ITEM_CODE: TStringField
      FieldName = 'ITEM_CODE'
      Origin = 'ITEM_CODE'
      ReadOnly = True
      Size = 41
    end
    object FDQuery1DESC1: TStringField
      FieldName = 'DESC1'
      Origin = 'DESC1'
      Size = 50
    end
    object FDQuery1P_UNIT: TStringField
      Alignment = taCenter
      FieldName = 'P_UNIT'
      Origin = 'P_UNIT'
      Required = True
      FixedChar = True
      Size = 4
    end
    object FDQuery1CANTIDAD: TIntegerField
      Alignment = taCenter
      FieldName = 'CANTIDAD'
      Origin = 'CANTIDAD'
    end
    object FDQuery1EXIST: TIntegerField
      FieldName = 'EXIST'
      Origin = 'EXIST'
    end
    object FDQuery1COSTO: TFloatField
      FieldName = 'COSTO'
      Origin = 'COSTO'
      DisplayFormat = ',0.00'
    end
    object FDQuery1PAGE_NO: TIntegerField
      FieldName = 'PAGE_NO'
      Origin = 'PAGE_NO'
      ReadOnly = True
      Required = True
    end
    object FDQuery1ULT_VTA: TSQLTimeStampField
      FieldName = 'ULT_VTA'
      Origin = 'ULT_VTA'
    end
    object FDQuery1VTA_PROM: TIntegerField
      FieldName = 'VTA_PROM'
      Origin = 'VTA_PROM'
    end
    object FDQuery1USERNAME: TStringField
      FieldName = 'USERNAME'
      Origin = 'USERNAME'
      Size = 15
    end
    object FDQuery1OFERTA: TIntegerField
      FieldName = 'OFERTA'
      Origin = 'OFERTA'
    end
    object FDQuery1RANK_NO: TIntegerField
      FieldName = 'RANK_NO'
      Origin = 'RANK_NO'
    end
    object FDQuery1RANK_PCT: TBCDField
      FieldName = 'RANK_PCT'
      Origin = 'RANK_PCT'
      Precision = 18
      Size = 2
    end
    object FDQuery1DESCTO_CMP: TIntegerField
      FieldName = 'DESCTO_CMP'
      Origin = 'DESCTO_CMP'
      ReadOnly = True
      Required = True
    end
    object FDQuery1QTY_OFERTA: TIntegerField
      Alignment = taCenter
      FieldName = 'QTY_OFERTA'
      Origin = 'QTY_OFERTA'
    end
    object FDQuery1PCT_BENEF: TBCDField
      FieldName = 'PCT_BENEF'
      Origin = 'PCT_BENEF'
      Precision = 18
      Size = 2
    end
    object FDQuery1PCT_BENEFTOTAL: TIntegerField
      FieldName = 'PCT_BENEFTOTAL'
      Origin = 'PCT_BENEFTOTAL'
      ReadOnly = True
      Required = True
    end
    object FDQuery1ITEM_STATUS: TStringField
      FieldName = 'ITEM_STATUS'
      Origin = 'ITEM_STATUS'
      FixedChar = True
      Size = 1
    end
    object FDQuery1F_MERCADO: TIntegerField
      FieldName = 'F_MERCADO'
      Origin = 'F_MERCADO'
      ReadOnly = True
    end
    object FDQuery1IND_OFERTA: TIntegerField
      FieldName = 'IND_OFERTA'
      Origin = 'IND_OFERTA'
      ReadOnly = True
      Required = True
    end
    object FDQuery1QTY_ORDEN: TIntegerField
      FieldName = 'QTY_ORDEN'
      Origin = 'QTY_ORDEN'
      ReadOnly = True
      Required = True
    end
    object FDQuery1MES: TIntegerField
      FieldName = 'MES'
      Origin = 'MES'
      ReadOnly = True
    end
    object FDQuery1YY: TIntegerField
      FieldName = 'YY'
      Origin = 'YY'
      ReadOnly = True
    end
    object FDQuery1PCTDESCTONEGO: TIntegerField
      FieldName = 'PCTDESCTONEGO'
      Origin = 'PCTDESCTONEGO'
      ReadOnly = True
      Required = True
    end
    object FDQuery1FACTOR: TIntegerField
      FieldName = 'FACTOR'
      Origin = 'FACTOR'
      ReadOnly = True
      Required = True
    end
    object FDQuery1TOTAL_PROD: TFloatField
      FieldName = 'TOTAL_PROD'
      Origin = 'TOTAL_PROD'
      ReadOnly = True
      DisplayFormat = ',0.00'
    end
    object FDQuery1BARCODE: TStringField
      FieldName = 'BARCODE'
      Origin = 'BARCODE'
      Size = 15
    end
    object FDQuery1LOCK_BIN_NO: TIntegerField
      FieldName = 'LOCK_BIN_NO'
      Origin = 'LOCK_BIN_NO'
    end
    object FDQuery1CO_NAME: TStringField
      FieldName = 'CO_NAME'
      Origin = 'CO_NAME'
      Size = 30
    end
    object FDQuery1RNC: TStringField
      FieldName = 'RNC'
      Origin = 'RNC'
      Size = 11
    end
    object FDQuery1CO_PHONE1: TStringField
      FieldName = 'CO_PHONE1'
      Origin = 'CO_PHONE1'
      Size = 12
    end
    object FDQuery1CO_PHONE2: TStringField
      FieldName = 'CO_PHONE2'
      Origin = 'CO_PHONE2'
      Size = 12
    end
    object FDQuery1CO_FAX: TStringField
      FieldName = 'CO_FAX'
      Origin = 'CO_FAX'
      Size = 12
    end
    object FDQuery1ADDR1: TStringField
      FieldName = 'ADDR1'
      Origin = 'ADDR1'
      Size = 60
    end
    object FDQuery1CITY: TStringField
      FieldName = 'CITY'
      Origin = 'CITY'
      Size = 50
    end
  end
end
