program OrdenVentaMail;

uses
  Vcl.Forms,
  Main in 'Main.pas' {Fmain},
  data in 'data.pas' {dm: TDataModule};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(Tdm, dm);
  Application.CreateForm(TFmain, Fmain);
  Application.Run;
end.
